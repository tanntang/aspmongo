using aspmongo.Models;
using aspmongo.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace aspmongo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {   
        private readonly AnimalsService _animalsService;

        public AnimalsController(AnimalsService animalsService)
        {
            _animalsService = animalsService;
        }

        [HttpGet]
        public ActionResult<List<Animals>> Get() =>
            _animalsService.Get();

        //[HttpGet("{number:length(10)}", Name = "GetAnimal")]
        [HttpGet("{number}", Name = "GetAnimal")]
        //public ActionResult<Animals> Get(string name)
        public ActionResult<Animals> Get(int number)
        {
            var animal = _animalsService.Get(number);
            if (animal == null)
            {
                return NotFound();
            }
            return animal;
        }

        [HttpPost]
        //public ActionResult<Animals> Create(Animals animal)
        public ActionResult<Animals> Create([FromForm] Animals animal)
        {
            _animalsService.Create(animal);
            //return animal;
            //return CreatedAtRoute("GetAnimal", new {_id = animal._id.ToString()}, animal);
            return CreatedAtRoute("GetAnimal", new {number = animal.number.ToString()}, animal);
        }

        [HttpDelete]
        public ActionResult<Animals> Delete([FromForm] int number)
        {
            var animal = _animalsService.Get(number);
            if (animal == null)
            {   
                return NotFound();
            }
            _animalsService.Remove(number);
            return animal;
        }
        

        /*
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Animals animalIn)
        {
            var animal = _animalsService.Get(id);

            if (animal == null)
            {
                return NotFound();
            }

            _animalsService.Update(id, animalIn);

            return NoContent();
        }
        */
    }
}