namespace aspmongo.Models
{
    public class ZooDatabaseSettings : IZooDatabaseSettings
    {
        public string AnimalsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IZooDatabaseSettings
    {
        string AnimalsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}