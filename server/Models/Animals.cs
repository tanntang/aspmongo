using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspmongo.Models
{
    public class Animals
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public int number { get; set; }

        public string name { get; set; }

        public decimal width { get; set; }

        public decimal height { get; set; }

        public decimal weight { get; set; }
    }
}