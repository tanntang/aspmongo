using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspmongo.Models
{
    public class Cols
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public int a { get; set; }
    }
}