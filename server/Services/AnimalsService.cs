using aspmongo.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace aspmongo.Services
{
    public class AnimalsService
    {
        private readonly IMongoCollection<Animals> animalsCollection;

        public AnimalsService(IZooDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            animalsCollection = database.GetCollection<Animals>(settings.AnimalsCollectionName);
        }

        public List<Animals> Get() =>
            animalsCollection.Find(animals => true).ToList();

        /*public Animals Get(string name) =>
            animalsCollection.Find<Animals>(animals => animals.name == name).FirstOrDefault();*/
        public Animals Get(int number) =>
            animalsCollection.Find<Animals>(animals => animals.number == number).FirstOrDefault();

        
        public Animals Create(Animals animal)
        {
            animalsCollection.InsertOne(animal);
            return animal;
        }

        public void Remove(int number) => 
            animalsCollection.DeleteOne(animal => animal.number == number);

        //other method
        /*
        public void Update(string id, Animals animalIn) =>
            animalsCollection.ReplaceOne(animal => animal._id == id, animalIn);
        public void Remove(Animals animalIn) =>
            animalsCollection.DeleteOne(animal => animal._id == animalIn._id);
        public void Remove(string id) => 
            animalsCollection.DeleteOne(animal => animal._id == id);
        */
    }
}